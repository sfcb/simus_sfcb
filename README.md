# Companion code repository for "A random forest approach for interval selection in functional regression"

## Usage

Scripts and their outputs are available in directory `RLib`. Data can be downloaded at https://doi.org/10.57745/KMH2GP (see also the file `data/README`).

- The script `simulated_comparison` corresponds to results of Section 4.1 of the manuscript.

- The script `truffles_comparison` corresponds to results of Section 4.2 of the manuscript.

- The scripts `systematic_simulations_data_generation` and `systematic_simulations_results` correspond to results of Section 4.3 of the manuscript.


## Citation

**Reference**: Servien, R., & Vialaneix, N. (2024). A random forest approach for interval selection in functional regression. *Statistical Analysis and Data Mining*, **17**(4), e11705. DOI: http://dx.doi.org/10.1002/sam.11705

This repository is a replication repository for this article.


## Authors

Rémi Servien, INRAE, Univ Montpellier, LBE, 11100 Narbonne, France

Nathalie Vialaneix, Université Fédérale de Toulouse, INRAE, MIAT, 31326 Castanet-Tolosan, France

For information about the repository, please contact nathalie.vialaneix@inrae.fr.


## License

The content of this repository is released under GPL v3 (or later).